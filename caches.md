# Cache Designs

There are so many components to a memory model. I don't need to model all of them, but I need to understand them to be able to make the decision.

## Levels
- Level 1 (VIPT) is flushed with context switch
- Level 2+ (PIPT) is coloured

## MMU is alongside L1 (VP) cache

## Address
- Tag (Physical), Set, Byte (offset)

## Line
- 32-64 bytes
- unit of read/write from cache

## Set
- Where a line will be written. each address maps to one set (using index part of address)
- Each set contains multiple lines (8 lines for 8-way associativity etc)
- Each line in the set is compared with the Tag to figure out which one is correct.

## Pages
- Page Number / Offset (vs)
- Tag / Index / Offset
- If page > tag, then the lowest bits of a page influence which sets can be written to.
- Overlapping by 1 bit = two colours. Overlapping by 3 bits = three colours. etc.
- This is cache colouring.

## Replacement policies
- LRU
- FIFO
- Random
- Do we need to model this kind of behaviour?

### Things to look into
- Different physical addresses between parts of hardware?
    - Related: Reto's work.
- Page tables.
    - Gilles-Barthe assumes a single-level page table, and assume one page table per application per (guest) operating system.
- 

## VIPT PIPT PIVT VIVT
- Virtually Indexed, Physically Tagged. etc.
- This means there are tags. Brush up on how that all works.

## TLB
- Translation Lookaside Buffer
    - For translating virtual addresses to physical addresses.
    - Page tables are in memory, so looking up these translations can be as slow as multiple memory lookups (are page tables cached differently to user memory?)
    - There are ITLBs and DTLBs (Instruction and Data, I think)
    - 