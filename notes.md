# Cache Model Lit Review Notes

Our goal is a generic model. Exactly how much specificity we put in there is as-yet undetermined.

## Goal
- These are the main pieces of work
- Most of them aren't relevant
- The ones that are relevant:
    - These are the big design questions (and how they answered them)
    - These are the things that may be useful to us
    - These are the unanswered questions


### So much of this work seems to be about hypervisors. I guess they simulate some cache hardware dont they. But that's not the same as what we're doing.


## Other Papers
### Probably Relevant
- "Crafting a Usable Microkernel, Processor, and I/O System with Strict and Provable Information Flow Security" (Mohit Tiwari, Jason K Oberg, Xun Li, Jonathan Valamehr, Timothy Levin, Ben Hardekopf, Ryan Kastner, Frederic T. Chong, Timothy Sherwood)
    - "This skeleton couples a critical slice of the low level hardware implementation with a microkernel in a way that allows information flow properties of the entire construction to be statically verified all the way down to its gate-level implementation."
    - "The problem with verifying information flows through high-level specifications (hand-written or otherwise) is that these models often ignore predictors, caches, buffers, timing variations, and undocumented/unspecified instruction behaviors that are not part of the ISA-level specification of the architecture. These structures greatly complicate accurate information flow evaluations because they can be used to infer the values of secret keys [8, 19], to subvert documented protection mechanisms [28], and to covertly transmit information between compartments [23]."
    - Actually appears to be a real model of caches etc.
    - "The fundamental problem is that the cache controller uses both secret and unclassified information to decide which cache lines to evict."
    - They seem to be doing bit-level analysis; we are much higher level.
    - Actually, they are implementng a new CPU architecture, with a model and proofs of protection from timing attacks.
    - I think I need to spend more time on this paper to understand their *model* better.
- "Verifying Shadow Page Table Algorithms" (Eyad Alkassar, Ernie Cohen†, Mark Hillebrand, Mikhail Kovalev, and Wolfgang J. Paul)
    - They model and verify a STLB
    - Only four pages
    - Designed for x64, but their model is more generic
    - Seems very low-level, with a focus on page walks
    - The model is a transition system on hardware configurations
    - Perhaps compare this with the TLB work from ??? at TS
    - At the end of the day, this is about _shadow_ page tables, which I believe we are not concerned with. So perhaps this is not relevant.

### Probably Not Relevant
- "Formal Memory Models for the Verification of Low-Level Operating-System Code" (Hendrik Tews, Marcus Völp, Tjark Weber)
    - "Caches for real memory are working completely transparent and can be ignored."
- "Non-Monopolizable Caches: Low-Complexity Mitigation of Cache Side Channel Attacks" (Leonid Domnitser, Aamer Jaleel, Jason Loew, Nael Abu-Ghazaleh and Dmitry Ponomarev)
    - An alternative protection against temporal attacks
    - Dynamically assigns reserved cache lines to threads, eliminating forced evictions.
    - Not about modeling, but relevant to our higher goals.
    - There are a number of approaches, not just this one and ours (GB references 6 papers)
- "Machine Assisted Proof of ARMv7 Instruction Level Isolation Properties" (Narges Khakpour, Oliver Schwarz, and Mads Dam)
    - "Our model does not include caches, timing or hardware extensions such as TrustZone or virtualization support."
- "Innovative Instructions and Software Model for Isolated Execution" (Frank McKeen, Ilya Alexandrovich, Alex Berenzon, Carlos Rozas, Hisham Shafi, Vedvyas Shanbhogue and Uday Savagaonkar)
    - An analysis of Intel SGX.
    - Hardware-supported virtualisation.
    - Not modeling really.




## Gilles Barthe
- "System-Level Non-interference of Constant-Time Cryptography. Part I: Model" (2019)
    - related: "System-level non-interference for constant-time cryptography" (2014)
- "System-Level Non-interference of Constant-Time Cryptography. Part II: Verified Static Analysis and Stealth Memory" (2020)
- "Cache-leakage resilient OS isolation in an idealized model of virtualization" (2012)
- "Formally Verifying Isolation and Availability in an Idealized Model of Virtualization" (2011)


### Co-Authors (to look into)
- Gustavo Betarte
    - formal verification, but no hardware stuff outside GB
- Juan Diego Campo
    - Just collabs with above
- Carlos Luna
    - Some temporal logic work
    - Otherwise just collabs with above
- David Pichardie
    - Lots of formal verification stuff
    - "A Certified Lightweight Non-interference Java Bytecode Verifier"
    - "A verified information-flow architecture" (With ben pierce! seems pretty theoretical though)
    - "An Extended Buffered Memory Model With Full Reorderings" at a glance this seems to ignore caching, but i should read more to see what exactly they model. It certainly is about formally modeling memory hardware.


### System-Level Non-interference of Constant-Time Cryptography. Part I: Model
- Problem stmt: "Thus, there is a need for complementary approaches where verification is performed on idealized models that abstract away from the specifics of any particular hypervisor, and yet provide a realistic setting in which to explore the security issues that pertain to the realm of hypervisors."
- Their focus: "Several countermeasures have been proposed for these attacks. One approach is to develop applications that do not leak information through the cache, such as making implementations constant-time i.e. programs whose execution time does not depend on the value of secrets. For our purposes, this will amount to **requiring that programs do not perform branches or memory accesses (such as table lookups) that _depend on secrets_.**"
  - Their model is for proving that their user (crypto library) level code does not allow for leaks. This is different to our goal.
- Developed a large model of shared hardware:
    - virtual and physical addressing
        - they use VIPT
    - memory mappings
    - page tables (single-level, one PT per application (domain?))
        - "While modern architectures mainly use multi-level page tables, we believe this simplification is reasonable and our approach can be generalized to this setting relatively easily."
    - TLB
    - cache
- 30,000 Coq SLoC (10,000 for model alone)
- A transition system with states and state transformers (actions). Defined using axiomatic semantics, w/ pre- and post-conds.
- States contain memory model including TLB and caches.
- Things they do not model:
    - PIPT (they only model VIPT)
    - The instruction cache.
    - Transparent page sharing (that's fine)
    - Devices.
    - Interrupts in general.
    - offsets of memory access. they model only pages.

## GB's model
### Machine Memory
- Represents actual memory
- Uses page classification for virtualisation purposes
- Each page has exactly one owner
- Each page is either data (r/w) or a page table
- These addresses are never directly used by guest OS (user)

### Physical Memory
- This is the guest OS's memory.
- It's mapped, so it's like virtual memory but for an OS.

### Virtual Addresses
- For each thread etc in the guest OS.
- OS needs to make calls to hypervisor to manage these addresses and PTs.

- Each OS has some hypervisor space in its memory, so the hypervisor can access its own data without making a 'context switch'.

### Addresses
- Consists of two parts:
    - page address
    - offset
- **They do not distinguish between offsets**
    - This means that only the _page_ that is beign accessed is reasoned about, not the offset.
    - Assuming that cache leakage might only be on the line level (confirm this), we can do the same.
- Page tables managed by hypervisor. One table per application per OS.
- Page tables modeled abstractly.
    - "The model must guarantee the correctness of those mappings, namely, that every machine address mapped in a page table of an OS is owned by it. We consider an abstract type of values equipped with an equality relation, and we assume given a distinguished value ⊥ when the value is undefined. In particular we abstract away implementation details such as encoding, size, etc."

### Caches
- Mapping from addresses to cache indexes
- There is some set associativity
- They keep a cache access history (for replacement policies)
    - Thinking about this for us, I think since we are arguing separation we should be fine here, and be able to ignore this part of the model.
- They generalise replacement policies, assuming only determinism over access history.
    - This isn't true for random replacement, so they talk about pseudorandom replacement. I'm pretty sure that a solid model would work even with 'true' random (unpredictable) evictions, although of course this would make some proofs more difficult.

### TLB
- Flushed on context switches
- Updated live with page tables, so never dirty + no writeback

### Replacement Policies
- I think we can ignore these, as our system should ensure that a domain only ever evicts its own cache lines.


### Things that we should probably model
- Virtual addresses
- ASIDs (any other model for security separation?)
- How are PTs treated in cache?
- Machine addresses
- Virtual addresses
- Caches (how many levels?)
- TLB + Page walking?
- Either VIPT or VIVT etc
- 


### Picture I'm getting of what our model would look like:
- L1 : fully flushed, VIPT
- TLB alongside L1
- L2+: coloured, PIPT
- Granularity: cache lines
- Address made of sections (page/offset and tag/set/offset?)
- The map from virtual address (or physical address) to cache address (set address)

