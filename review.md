# Review
Overall, Gilles Barthe's work seems to be the only real work on cache modeling.
The closest other work is from Tiwari et al, which does model infoflow including caches, but on a very low level (logic gates), and using a lot of automation.

Gilles Barthe's work is spread over a number of papers [make list], but has been summarised in two more recent longer (51/45 pgs) papers:
- "System-Level Non-interference of Constant-Time Cryptography. Part I: Model" (2019)
- "System-Level Non-interference of Constant-Time Cryptography. Part II: Verified Static Analysis and Stealth Memory" (2020)

The first of these papers contains their model, which is what we are most interested in at this stage.

### GB's goal
Provable safety from timing channel attacks.
This is only accounting for constant-time software: "For our purposes, this will amount to **requiring that programs do not perform branches or memory accesses (such as table lookups) that _depend on secrets_.**"
They are verifying the time-security of software that is written specifically for time-security.
We are aiming to verify time-securify between two untrusted executions.
This is a significant difference.

### GB's Model
Gilles Barthe's model is mechanised in Coq (10k lines for the model another 20k of proofs).
They only model VIPT (so single-level cache), and a single-level page table.
The system is a transition system between machine states.
A machine state includes the state of the memory including caches and the TLB.

They model machine memory, classifying each page. Page tables are classified separately from data pages.
Virtual memory is mapped to this machine memory.

For virtualisation, they have the levels MACHINE / PHYSICAL / VIRTUAL. This is to separate an OS's view of machine memory from real machine memory.

In caches, they keep a history of cache accesses (will we do this? I don't think so). They don't care about replacement policies (nor will we I think).

Their TLB is flushed with context switches. What is our model for TLBs?

## Questions remaining
- Where do page tables sit in our colouring scheme? How does the TLB's caching of page tables work with this?
- Are we concerned about TLB actions in our view of time protection? Is the TLB flushed?
- Is the ASID our model of a 'domain'? Is the ASID what is used for cache colouring? If not, how is the colour defined?

## My Assumptions
- Multi-level cache, L1 is VIPT, rest is PIPT.
- Flushing L1 on context switch - do we need to model this?
- Colouring on L2+.
- We only care about addresses down to the cache line level - which set an access uses. Offsets are not relevant.
